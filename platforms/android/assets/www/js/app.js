(function () {
    var app = angular.module('voteingapp', ['angular-storage', 'ionic', 'ngAnimate', 'ngCordova', 'ngResource', 'ngSanitize', 'satellizer', 'emguo.poller', 'timer', 'ngOpenFB', 'ngCordovaOauth'])

    app.run(function ($ionicPlatform, $rootScope, $state, ngFB, $ionicLoading, $auth, $timeout) {
        $ionicPlatform.ready(function () {
            $ionicPlatform.ready(function () {

                // cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                //  }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
                //
                ngFB.init({
                    appId: '1640396249544172'
                })


            });

            $rootScope.$on('error', function (event, networkState) {

                var stoploading = function () {

                    $ionicLoading.hide();
                    toastr.error('Invaild user and password', 'Error')
                }
                $timeout(stoploading, 3000);

            })

            $rootScope.$on('nonetwork', function (event, networkState) {

                var stoploading = function () {

                    $ionicLoading.hide();
                    toastr.error('You are not Conected', 'Error')
                }
                $timeout(stoploading, 3000);

            })

            $rootScope.$on('status', function (event) {
                console.log('done');
                $ionicLoading.hide();
            })
            $rootScope.$on('loading', function (event) {
                console.log('loading');
                $ionicLoading.show({
                    template: '<div> Loading <br> <ion-spinner icon="lines"/></div>',
                    duration: 10000
                });
            })

            $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
                var onlineState = networkState;

                $ionicLoading.hide();
            })

            // listen for Offline event
            $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
                var offlineState = networkState;

                $ionicLoading.show({
                    template: 'No Internet Connection...'
                });
                console.log(offlineState);
            })



            $rootScope.$on("$stateChangeStart", function (event, next, current) {

                var restrictedPage = $.inArray($state.current.url, ['/login', '/index', '/register', ""]) === -1;



                if (!$auth.isAuthenticated() && !restrictedPage) {

                    $state.go('index');

                }
            });
        });
    })

    app.config(function ($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $ionicConfigProvider, $cordovaInAppBrowserProvider) {
        // Satellizer configuration that specifies which API
        // route the JWT should be retrieved from
        $authProvider.loginUrl = 'http://winninriddim.co/api/index.php/api/authenticate';

        $stateProvider

            .state('tabs', {
            url: "/tab",
            abstract: true,
            templateUrl: "templates/tabs.html"
        })


        .state('index', {
            url: '/index',
            controller: 'indexController',
            templateUrl: 'templates/index.html'
        })

        .state('register', {
            url: '/register',
            controller: 'registerController',
            templateUrl: 'templates/register.html'
        })


        .state('login', {
                url: '/login',
                controller: 'LoginController',
                templateUrl: 'templates/login.html'
            })
            .state('tabs.home', {
                url: "/home",
                views: {
                    'home-tab': {
                        templateUrl: 'templates/home.html',
                        controller: 'RiddmidController'
                    }
                }
            })
            .state('tabs.iconic', {
                url: "/iconic",
                views: {
                    'fav-tab': {
                        templateUrl: 'templates/iconic.html',
                        controller: 'iconicController'
                    }
                }
            })

        .state('vote', {
                url: '/vote/:id',
                templateUrl: 'templates/vote.html',
                controller: 'voteController'
            })
            .state('Composer', {
                url: '/Composer/:id',
                templateUrl: 'templates/composer.html',
                controller: 'ComposerController'
            })
            .state('Setting', {
                url: '/Setting/',
                templateUrl: 'templates/Setting.html',
                controller: 'SettingController'
            })

        var defaultOptions = {
            location: 'no',
            clearcache: 'no',
            toolbar: 'yes',
            closebuttoncaption: 'Close'
        };



        $cordovaInAppBrowserProvider.setDefaultOptions(defaultOptions)






        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise("/tab/home");



        $ionicConfigProvider.tabs.position("top"); //Places them at the bottom for all OS

        $httpProvider.interceptors.push('httpInterceptor');

    });









})();
