(function () {

    var app = angular.module('voteingapp')

    app.controller('indexController', function ($scope, $auth, $state, ngFB, $auth, store, $http, $cordovaOauth, RiddmidService) {


        //$auth.logout();
        //$auth.removeToken();
        $scope.gotoLogin = function () {
            $state.go('login');

        }


        $scope.gotoregister = function () {

            $state.go('register');

        }

        $scope.fbLogin = function () {


            ngFB.login({
                scope: 'email,publish_actions,user_friends,user_posts'
            }).then(
                function (response) {
                    if (response.status === 'connected') {
                        ngFB.api({
                            path: '/me',
                            params: {
                                fields: 'id,first_name,last_name,email,gender,picture'
                            }
                        }).then(
                            function (user) {

                                store.set('ProfileULR', user.picture.data.url)
                                console.log(user)
                                fbregister(response, user);

                            },
                            function (error) {
                                console.log(error);
                            });
                    } else {
                        console.log(response);
                    }
                });

            function fbregister(user, data) {
                console.log(data)

                RiddmidService.Create(data)
                    .then(function (response) {
                        if (response.token) {
                            $auth.setToken(response.token);
                            store.set('user', response.userdata);
                            $state.transitionTo('tabs.home');

                            console.log(response)
                        } else {
                            $scope.errors = response;
                            console.log($scope.errors);
                        }
                    });
            }






            //            $cordovaOauth.facebook("1640396249544172", ["email", "public_profile"]).then(function (result) {
            //
            //
            //
            //                $http.get("https://graph.facebook.com/v2.5/me", {
            //                    params: {
            //                        access_token: result.access_token,
            //                        fields: "first_name,last_name,gender,email",
            //                        format: "json"
            //                    }
            //                }).then(function (result) {
            //                    $auth.setToken(result.access_token);
            //                    $state.transitionTo('tabs.home');
            //
            //                }, function (error) {
            //
            //                    console.log(error);
            //                });
            //
            //
            //            }, function (error) {
            //                console.log(error)
            //
            //            });
            //
            //
            //
            //
            //

        };


    })



    app.controller('LoginController', function ($scope, $auth, $state, store, $cordovaNetwork, $ionicLoading, $timeout, $rootScope, $cordovaToast) {

        $scope.loginData = {};

        $scope.isloading = false



        $scope.doLogin = function () {

            $ionicLoading.show({
                template: '<div> Signing in <br> <ion-spinner icon="crescent"/></div>'
            });


            $scope.isloading = true;

            var credentials = {
                email: $scope.loginData.email,
                password: $scope.loginData.password
            }

            $auth.login(credentials).then(function (data) {

                // If login is successful, redirect to the users state
                $ionicLoading.hide();
                $state.transitionTo('tabs.home');
                event.preventDefault();


                store.set('user', data.data.userdata);
            });






        }


        $scope.gotoregister = function () {
            $state.transitionTo('register');
        }


    });
    app.controller('ComposerController', function ($scope, $stateParams, store, RiddmidService, $cordovaSocialSharing, $ionicLoading) {

        var isWebView = ionic.Platform.isWebView();
        var isIOS = ionic.Platform.isIOS();
        var isAndroid = ionic.Platform.isAndroid();
        var isWindowsPhone = ionic.Platform.isWindowsPhone();

        $scope.composer = {};

        $scope.pageurl = "Composer Profile";

        $ionicLoading.show({
            template: '<div> Loading <br> <ion-spinner icon="crescent"/></div>'
        });


        RiddmidService.GetById($stateParams.id)
            .then(function (response) {
                $scope.composer = response[0];

                console.log($scope.composer)

                $ionicLoading.hide();
            });








        $scope.sharetofacebook = function () {

            message = "I just Checked out " + $scope.composer.Trackname + " by" + $scope.composer.Composername +
                "on the Winnin Riddim Pepsi app";
            image = $scope.ComposerProfilepic;
            link = "https://play.google.com/store/apps/details?id=com.Sretlawmedia.WinninRiddim";


            if (isIOS) {
                link = "https://itunes.apple.com/us/app/winnin-riddim/id1082746748?mt=8";
            }

            $cordovaSocialSharing
                .shareViaFacebookWithPasteMessageHint(message, image, link)
                .then(function (result) {
                    // Success!
                }, function (err) {
                    console.log(err)
                });




        }

        $scope.sharetotwitter = function () {

            message = "I just Checked out " + $scope.composer.Trackname + " by" + $scope.composer.Composername +
                "on the Winnin Riddim Pepsi app";
            image = $scope.ComposerProfilepic;
            link = "https://play.google.com/store/apps/details?id=com.Sretlawmedia.WinninRiddim";


            if (isIOS) {
                link = "https://itunes.apple.com/us/app/winnin-riddim/id1082746748?mt=8";
            }

            $cordovaSocialSharing
                .shareViaTwitter(message, image, link)
                .then(function (result) {
                    // Success!
                }, function (err) {
                    console.log(err)
                });


        }

    });
    app.controller('voteController', function ($scope, $stateParams, store, clockdata, RiddmidService, poller, $state, $timeout, $ionicLoading, $rootScope, PlayService, $interval) {

        $scope.pageurl = "Riddim";

        $scope.time = 3600;

        $scope.mediaDuration = 100;

        $scope.counter = '00';

        $scope.countsec = '00';

        $scope.mediaPostion = 0;

        $scope.notplaying = true;

        var media;


        var playingmedia = store.get('media');

        var vewingmedia = store.get('mediavoteurl');


        console.log(playingmedia);


        if (playingmedia != null) {



            if (angular.equals(playingmedia.src, vewingmedia)) {

                if (playingmedia._duration != -1) {

                    var mediaDuration = PlayService.GetDuration(playingmedia);

                    $scope.mediaDuration = mediaDuration.toPrecision(2);



                    $interval(callAtInterval, 1000);


                    function callAtInterval() {
                        PlayService.GetCurrentPosition(playingmedia);

                        $scope.mediaPostion = store.get('position')

                        $scope.mediaPostion = $scope.mediaPostion.toPrecision(2);

                        console.log($scope.mediaPostion)


                    }

                    $scope.playpause = function () {



                        if ($scope.notplaying) {

                            $scope.notplaying = false;

                            if (angular.equals(playingmedia.src, vewingmedia)) {

                                PlayService.Pause(playingmedia);

                            } else {

                            }
                        } else {

                            PlayService.Play(playingmedia);


                            $scope.notplaying = true;

                        }

                    }
                } else {


                    $scope.playpause = function () {


                        media = PlayService.Load(vewingmedia);




                        if ($scope.notplaying) {


                            PlayService.Play(media);

                            store.set('playing', media);

                            $scope.notplaying = false;

                            var mediaDuration = PlayService.GetDuration(media);

                            $scope.mediaDuration = mediaDuration.toPrecision(2);



                            $interval(callAtInterval, 1000);


                            function callAtInterval() {
                                PlayService.GetCurrentPosition(media);

                                $scope.mediaPostion = store.get('position')

                                $scope.mediaPostion = $scope.mediaPostion.toPrecision(2);

                                console.log($scope.mediaPostion)


                            }







                        } else {


                            PlayService.Pause(store.get('playing'));
                            $scope.notplaying = true;
                        }

                    }


                }









            } else {

                if (playingmedia.src) {

                    PlayService.Stop(playingmedia);

                }


            }


        } else {


            $scope.playpause = function () {


                media = PlayService.Load(vewingmedia);




                if ($scope.notplaying) {


                    PlayService.Play(media);

                    store.set('playing', media);

                    $scope.notplaying = false;

                    var mediaDuration = PlayService.GetDuration(media);

                    $scope.mediaDuration = mediaDuration.toPrecision(2);



                    $interval(callAtInterval, 1000);


                    function callAtInterval() {
                        PlayService.GetCurrentPosition(media);

                        $scope.mediaPostion = store.get('position')

                        $scope.mediaPostion = $scope.mediaPostion.toPrecision(2);

                        console.log($scope.mediaPostion)


                    }







                } else {


                    PlayService.Pause(store.get('playing'));
                    $scope.notplaying = true;
                }

            }


        }






        // $scope.playpause = function () {

        //       $scope.counter = 59;

        //     if ($scope.notplaying) {

        //         $scope.notplaying = false;

        //         if (angular.equals(playingmedia.src, vewingmedia)) {

        //             PlayService.Pause(playingmedia);

        //         } else {

        //         }
        //     } else {

        //         PlayService.Play(playingmedia);


        //         $scope.notplaying = true;

        //     }

        // }




        $ionicLoading.show({
            template: '<div> Loading <br> <ion-spinner icon="crescent"/></div>'
        });



        if ($scope.counter < 60) {
            $scope.disable = true;

        }

        if ($scope.counter == '00') {
            $scope.disable = false;

        }



        RiddmidService.GetById($stateParams.id)
            .then(function (response) {

                $scope.riddmidname = response[0].Trackname;
                $ionicLoading.hide();



            });

        $scope.vote = function (item, event) {

            $scope.counter = 59

            $scope.timerRunning = true;

            $scope.disable = true;

            var votedata = {
                RiddimId: $stateParams.id,
                UserId: store.get('user')[0].userid,
                Point: item
            }


            RiddmidService.vote(votedata)
                .then(function (response) {

                    if (response[0].now) {

                        toastr.success('Thank You', 'Your vote was submitted')

                    } else {
                        toastr.error('Error', 'You have Already Voted')
                    }

                    console.log(response);


                });

            //store.set('time', data.data.userdata);
        }



        $scope.gotocomposer = function () {

            $state.go('Composer', {
                id: $stateParams.id
            }, {
                reload: true
            });

        }
        var gettime = {
            rid: $stateParams.id,
            uid: store.get('user')[0].userid
        }
        poller1 = poller.get(clockdata, {
            action: 'jsonp_get',
            delay: 10000,
            catchError: true
        });
        poller1.promise.then(function (error) {
            console.log(error)
        }, function (success) {}, function (data) {


            RiddmidService.Fecthvotetime(gettime)
                .then(function (response) {


                    var time = {
                        end: response[0].updatedat,
                        start: data[0].Systemdate
                    }



                    RiddmidService.Fecthvotetimediff(time)
                        .then(function (response) {


                            $scope.counttime = response[0].min;


                            console.log($scope.counttime)


                            if ($scope.counttime > 59) {
                                $scope.counter = 60
                                $scope.disable = false;

                            } else {
                                $scope.counter = 59 - $scope.counttime;

                                $scope.disable = true;
                            }




                        });

                });

        });
    });
    app.controller('RiddmidController', function ($scope, $auth, Riddmiddata, $state, poller, PlayService, $cordovaNetwork, $ionicLoading, store) {


        $scope.select = function (index) {

            $scope.selected = index;


        };



        $scope.pageurl = "Home";

        $scope.searchText = "";

        $scope.url = 'img/assets/Artboard 17.png';

        $scope.url1 = 'img/assets/play button.png';

        $scope.listCanSwipe = true;

        $scope.notplaying = true;

        if (!$auth.isAuthenticated()) {
            $state.transitionTo('index');
            event.preventDefault();
        }

        $scope.issreaching = function () {

            $scope.sreaching = true;
        }
        $scope.cancelsreaching = function () {

            $scope.sreaching = false;

            $scope.searchText = "";
        }

        poller1 = poller.get(Riddmiddata, {
            action: 'jsonp_get',
            delay: 120000,
            catchError: true
        });
        poller1.promise.then(function (error) {
            console.log(error)

        }, function (success) {

        }, function (data) {

            $scope.Riddmiddata = [];

            angular.forEach(data, function (values) {
                $scope.Riddmiddata.push(values);

            });

            $scope.registerData = {};



            $scope.gotovote = function (item, event) {

                $state.go('vote', {
                    id: item
                }, {}, {
                    reload: true
                });
            }

            $scope.storeurl = function (item, event) {

                store.set('mediavoteurl', item);


            }
            document.addEventListener("deviceready", function () {

                var type = $cordovaNetwork.getNetwork()

                var isOnline = $cordovaNetwork.isOnline()

                var isOffline = $cordovaNetwork.isOffline()

                $scope.play = function (item, event) {


                    media = PlayService.Load(item);

                    if ($scope.notplaying) {



                        $scope.notplaying = false;

                        // $scope.url = 'img/assets/Artboard 17.png';

                        PlayService.Play(media);

                        store.set('media', media);



                    } else {
                        //$scope.url = 'img/assets/play button.png';

                        $scope.notplaying = true;



                        PlayService.Stop(store.get('media'));

                    }


                }


            }, false);
        });

        $scope.GotoSetting = function () {
            $state.transitionTo('Setting');
        }
    });

    app.controller('iconicController', function ($scope, $auth, Riddmiddata, $state, poller, $cordovaNetwork, PlayService) {
        $scope.pageurl = "Iconic";


        $scope.searchText = "";

        $scope.url = 'img/assets/play button.png';

        $scope.listCanSwipe = true;

        $scope.notplaying = true;

        if (!$auth.isAuthenticated()) {
            $state.transitionTo('index');
            event.preventDefault();
        }

        $scope.issreaching = function () {

            $scope.sreaching = true;
        }
        $scope.cancelsreaching = function () {

            $scope.sreaching = false;

            $scope.searchText = "";
        }

        poller1 = poller.get(Riddmiddata, {
            action: 'jsonp_get',
            delay: 120000,
            catchError: true
        });
        poller1.promise.then(function (error) {
            console.log(error)

        }, function (success) {

        }, function (data) {

            $scope.Riddmiddata = [];

            angular.forEach(data, function (values) {
                $scope.Riddmiddata.push(values);

            });

            $scope.registerData = {};



            $scope.gotovote = function (item, event) {

                $state.go('vote', {
                    id: item
                });
            }

            $scope.storeurl = function (item, event) {

                store.set('mediavoteurl', item);


            }
            document.addEventListener("deviceready", function () {

                var type = $cordovaNetwork.getNetwork()

                var isOnline = $cordovaNetwork.isOnline()

                var isOffline = $cordovaNetwork.isOffline()

                $scope.play = function (item, event) {


                    media = PlayService.Load(item);

                    if ($scope.notplaying) {



                        $scope.notplaying = false;

                        // $scope.url = 'img/assets/Artboard 17.png';

                        PlayService.Play(media);

                        store.set('media', media);



                    } else {
                        $scope.url = 'img/assets/play button.png';

                        $scope.notplaying = true;



                        PlayService.Stop(store.get('media'));

                    }


                }


            }, false);
        });

        $scope.GotoSetting = function () {
            $state.transitionTo('Setting');
        }
    });

    app.controller('registerController', function ($scope, $ionicPopup, RiddmidService, $state, $auth, $ionicPopup, ngFB, store) {
        $scope.registerData = {};
        $scope.errors;
        $scope.doregister = function () {

                register($scope.registerData);
            }
            // $scope.fbLogin = function () {
            // ngFB.login({
            // scope: 'email,publish_actions,user_friends,user_posts'
            // }).then(
            // function (response) {
            // if (response.status === 'connected') {
            // ngFB.api({
            // path: '/me',
            // params: {
            // fields: 'id,first_name,last_name,email,gender'
            // }
            // }).then(
            // function (user) {
            // fbregister(response, user);
            // },
            // function (error) {
            // console.log(error);
            // });
            // } else {
            // console.log(response);
            // }
            // });
            // };

        function register(data) {

            RiddmidService.Create(data)
                .then(function (response) {

                    var credentials = {
                        email: $scope.registerData.email,
                        password: $scope.registerData.password
                    }

                    if (response.token) {

                        $auth.login(credentials).then(function (data) {


                            store.set('user', data.data.userdata);
                            $state.transitionTo('tabs.home');
                            event.preventDefault();
                        });
                    } else {

                        $scope.errors = response.email;
                        $scope.perrors = response.password_confirm;
                    }
                });
        }

        /*function fbregister(user, data) {

        RiddmidService.Create(data)
        .then(function (response) {
        if (response.token) {
        $auth.setToken(response.token);
        store.set('user', response.data.userdata);
        $state.transitionTo('tabs.home');
        } else {
        $scope.errors = response;
        console.log($scope.errors);
        }
        });
        }*/
    });
    app.controller('BackController', function ($scope, $ionicHistory, $state, PlayService, store) {

        $scope.myGoBackHome = function () {
            //$ionicHistory.goBack();

            if (store.get('playing') != null) {

                if (store.get('playing')._duration != -1) {

                    PlayService.Stop(store.get('playing'));

                }




            }

            console.log(store.get('playing'));

            $state.transitionTo('tabs.home');

        };

        $scope.myGoBackVote = function () {
            //ionicHistory.goBack();
            $state.transitionTo('tabs.home');
        };

        $scope.myGoBackindex = function () {
            $state.go('index');

        };



    });

    app.controller('TemrsController', function ($scope, $cordovaInAppBrowser) {


        var isIOS = ionic.Platform.isIOS();
        var isAndroid = ionic.Platform.isAndroid();


        if (isAndroid) {





            $scope.gotoTerms = function () {

                $cordovaInAppBrowser.open('http://docs.google.com/viewer?url=http://winninriddim.co/PrivacyPolicy.pdf', '_blank')
                    .then(function (event) {
                        // success
                    })
                    .catch(function (event) {
                        // error
                    });
            };



        } else if (isIOS) {




            $scope.gotoTerms = function () {

                $cordovaInAppBrowser.open('http://winninriddim.co/PrivacyPolicy.pdf', '_blank')
                    .then(function (event) {
                        // success
                    })
                    .catch(function (event) {
                        // error
                    });
            };






        }








    });
    app.controller('SettingController', function ($scope, $cordovaInAppBrowser, store, $state, $cordovaAppVersion) {


        $scope.pageurl = "Setting";

        $scope.Name = store.get('user')[0].firstName;

        $scope.Email = store.get('user')[0].email;

        $scope.Profile = store.get('ProfileULR');

        if (store.get('ProfileULR')) {
            $scope.facebook = true;
        }



        console.log(store.get('user'));

        $scope.goback = function () {


            $state.go('tabs.home');


        };

        document.addEventListener("deviceready", function () {
            $cordovaAppVersion.getVersionNumber().then(function (version) {
                $scope.Version = version;
            });
        }, false);









    });
})();
