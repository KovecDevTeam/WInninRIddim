(function () {
    var app = angular.module('voteingapp')

    app.factory('RiddmidService', RiddmidService);

    function RiddmidService($http, API, $auth) {
        var service = {};

        var api = API.url;

        service.GetAll = GetAll;
        service.GetById = GetById;
        //service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.vote = vote;

        service.Fecthvotetime = Fecthvotetime;

        service.Fecthvotetimediff = Fecthvotetimediff;

        service.GetWinner = GetWinner;
        //service.Delete = Delete;

        return service;

        function GetAll() {
            return $http.get(api + '/Riddmin').then(handleSuccess, handleError);
        }

        function Create(user) {
            return $http.post(api + '/api/User', user).then(handleSuccess, handleError);
        }

        function vote(vote) {
            return $http.post(api + 'api/Vote/', vote).then(handleSuccess, handleError);
        }


        function GetById(id) {
            return $http.get(api + 'api/Vote/' + id).then(handleSuccess, handleError());
        }

        function Fecthvotetime(id) {
            return $http.post(api + 'api/votetime/', id).then(handleSuccess, handleError());
        }

        function Fecthvotetimediff(id) {
            return $http.post(api + 'api/time/', id).then(handleSuccess, handleError());
        }

        function GetWinner() {
            return $http.get(api + 'api/Winner').then(handleSuccess, handleError());
        }
        /*
 
        function GetByUsername(username) {
            return $http.get('/api/users/' + username).then(handleSuccess, handleError('Error getting user by username'));
        }
 
        function Create(user) {
            return $http.post('/api/users', user).then(handleSuccess, handleError('Error creating user'));
        }
 
        function Update(user) {
            return $http.put('/api/users/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }
 
        function Delete(id) {
            return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
        }*/

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {

            return error;

        }
    }



    app.factory('Riddmiddata', function ($resource, API) {

        var api = API.url;

        var url = api + 'api/Riddmin';

        return $resource(url, {}, {
            jsonp_get: {
                method: 'GET',
                isArray: true
            }
        });

    })


    app.factory('clockdata', function ($resource, API) {

            var api = API.url;

            var url = api + 'api/clock';

            return $resource(url, {}, {
                jsonp_get: {
                    method: 'GET',
                    isArray: true
                }
            });

        })
        /*app.factory('getRiddmiddata', function ($resource,API) {

         var api=API.url;

       var url=api+'api/Riddmin';

   return $resource(url,{},{jsonp_get: { method: 'GET', isArray:true }});

})*/
    app.factory('PlayService', function ($cordovaMedia, $ionicLoading, $cordovaProgress, $rootScope, $timeout, store) {

        var isWebView = ionic.Platform.isWebView();
        var isIOS = ionic.Platform.isIOS();
        var isAndroid = ionic.Platform.isAndroid();
        var isWindowsPhone = ionic.Platform.isWindowsPhone();


        var iOSPlayOptions = {
            numberOfLoops: 0,
            playAudioWhenScreenIsLocked: true
        }

        var loadmusic = function (song) {

            var media = new Media(song, function () {
                console.log("playAudio():Audio Success");
            }, function (err) {
                console.log("playAudio():Audio Error: " + JSON.stringify(err));
            }, function (status) {
                console.log("playAudio():Audio Status: " + status);

                $rootScope.$broadcast('status')
            });

            return media;

        }

        if (isAndroid) {
            var doPlay = function (media) {



                $rootScope.$broadcast('loading')

                $timeout(function () {
                    media.play()
                }, 800);


            }
        } else if (isIOS) {
            var doPlay = function (media) {


                $rootScope.$broadcast('loading')

                $timeout(function () {
                    media.play(iOSPlayOptions)
                }, 800);








            }

        }

        var endPlay = function (media) {
            media.stop()

            console.log('Stopped')


        }

        var GetDuration = function (media) {

            return media.getDuration(media);


        }

        var Pause = function (media) {

            return media.pause();


        }


        var GetCurrentPosition = function (media) {

            media.getCurrentPosition(

                function (position) {
                    if (position > -1) {




                        store.set('position', position)


                    }
                },

                function (e) {
                    console.log("Error getting pos=" + e);
                });

        }





        return {
            Play: function (media) {
                return doPlay(media);
            },
            Stop: function (media) {
                return endPlay(media);
            },
            Load: function (song) {
                return loadmusic(song);
            },
            GetDuration: function (media) {
                return GetDuration(media);
            },
            GetCurrentPosition: function (media) {
                return GetCurrentPosition(media);
            },
            Pause: function (media) {
                return Pause(media);
            }
        };




    })

    app.factory('httpInterceptor', function ($q, $rootScope, $log, store) {
        return {
            request: function (config) {
                return config || $q.when(config)
            },
            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function (response) {
                if (response.status === 401) {

                    $rootScope.$broadcast('error')
                }
                if (response.status === 500) {

                    $rootScope.$broadcast('error')
                }
                if (response.status === 404) {

                    $rootScope.$broadcast('nonetwork')
                }
                return $q.reject(response);
            }
        };
    });




})();
